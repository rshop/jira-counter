const _browser = this._browser || this.browser || this.chrome;
const storage = _browser.storage.sync || _browser.storage.local;

function storeData() {
    const jira = document.getElementById('jira-url').value
    const jql = document.getElementById('jira-jql').value

    document.getElementById('jira-filter').href = jira + '/issues/?jql=' + encodeURIComponent(jql)
    storage.set(
        {
            jiraURL: jira,
            jql: encodeURIComponent(jql)
        },
        () => {
            window.setTimeout(() => {
                window.close();
            }, 1000);
        }
    );
}

const defaultDomain = 'https://riesenia.atlassian.net';
const defaultJql = 'assignee%20in%20membersOf(DEV-ROOM)%20and%20status%20in%20(%22In%20Progress%22,%20%22In%20Review%22,%20Testing)'

storage.get(['jiraURL', 'jql'], function (result) {
    let domain = result.jiraURL || defaultDomain
    let jql = result.jql || defaultJql

    document.getElementById('jira-url').value = domain
    document.getElementById('jira-jql').value = decodeURIComponent(jql)

    document.getElementById('jira-filter').href = domain + '/issues/?jql=' + jql
})

document.getElementById('form-save').addEventListener('click', function () {
    storeData()

    chrome.extension.getBackgroundPage().fetchJira();
});