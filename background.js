const defaultDomain = 'https://riesenia.atlassian.net';
const apiUrl = '/rest/api/2/search';
const fields = 'worklog,assignee,project,summary,issuetype,timespent,priority,timeoriginalestimate';
const expand = 'changelog';
const defaultJql = 'assignee%20in%20membersOf(DEV-ROOM)%20and%20status%20in%20(%22In%20Progress%22,%20%22In%20Review%22,%20Testing)'

const _browser = this.browser || this.chrome;
const storage = _browser.storage.sync || _browser.storage.local;

function fetchJira() {
    let domain, jql;

    storage.get(['jiraURL', 'jql'], function (result) {
        domain = result.jiraURL || defaultDomain
        jql = result.jql || defaultJql

        if (!jql || !domain) {
            chrome.browserAction.setBadgeText({ text: "0" })
            chrome.browserAction.setBadgeBackgroundColor({ color: "green" });
            return
        }

        fetchUrl(domain + apiUrl + '?fields=' + fields + '&expand=' + expand + '&jql=' + jql)
    })
}

function fetchUrl(url) {
    var myHeaders = new Headers();
    myHeaders.append("Cookie", "atlassian.xsrf.token=B2ZX-VMGZ-VIGP-DLU4_ce6c6674fb3e14c5827f266279c6997fb0c1349b_lout");
    myHeaders.append("X-Atlassian-Token", "no-check");
    myHeaders.append("Access-Control-Allow-Origin", "*");

    var requestOptions = {
        method: 'GET',
        headers: myHeaders,
        redirect: 'follow',
        mode: 'no-cors',
        accept: 'application/json'
    };

    fetch(url, requestOptions)
        .then(response => response.json())
        .then(result => {
            chrome.browserAction.setBadgeText({ text: '' + result.total })
            chrome.browserAction.setBadgeBackgroundColor({ color: "red" });
        })
        .catch(error => {
            chrome.browserAction.setBadgeText({ text: "ERROR" })
            chrome.browserAction.setBadgeBackgroundColor({ color: "yellow" });
        });
}

chrome.browserAction.setBadgeText({ text: "0" });

let interval = setInterval(() => {
    fetchJira()
}, 15 * 60 * 1000)

fetchJira()

chrome.extension.onRequest.addListener(
    function (request, sender, sendResponse) {
        chrome.pageAction.show(sender.tab.id);
        sendResponse({});
    }
);